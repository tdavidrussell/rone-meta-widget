=== Raging One Customized Meta Widget ===
Contributors: Russell
Donate link: www.ragingone.com/
Tags: widget, meta, custom
Requires at least: 5.4
Tested up to: 5.6
Stable tag: master

This widget plugin is like the default Meta widget, but customizable. (You can specify what links to display).

== Description ==

This plugin adds a widget, like the default Meta widget (but customizable). You can specify what links to display.

== Installation ==

1. Upload `rkmg-meta-widget` directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Use Appearance->Widgets menu, then to add to sidebar and edit settings.

== Frequently Asked Questions ==

= Where can I ask for help? =
    http://www.ragineone.com/wordpress-plugins/

== Screenshots ==

1. screenshot1.png
2. screenshot2.png
